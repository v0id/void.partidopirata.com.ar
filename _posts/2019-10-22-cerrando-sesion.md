---
layout: post
title: cerrando sesión... *click*
date: 2019-10-22 00:00:00 +0000
categories: desintoxicacion
---

english TL;DR: i'm leaving Telegram and Mastodon for now. feel free to email me.

me di cuenta de que quizás mi presencia en los interwebs me satura, me pone nervioso, me jode. decidí que voy a probar algo nuevo, sin tanta presión invisible, sin la necesidad de mirar el celular cada 5 segundos.

voy a abandonar, por ahora, mi cuenta de Telegram y Mastodon. les invito a contactarme, a lxs que antes lo hacian por estos medios, vía email (v0id arroba riseup punto net).

nos vemos o/
