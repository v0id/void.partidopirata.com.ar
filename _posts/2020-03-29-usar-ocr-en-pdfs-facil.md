---
layout: post
title: mandarle OCR a los PDFs fácilmente (pdfsandwich)
date: 2020-03-29 00:00:00 +0000
categories:
  - pdf
  - ocr
---

como están pasando esa quarenteen? a mi me mandaron PDFs de papel escaneado para cosas legales, y necesitaba buscar dentro del texto algunas partes. sin embargo, el escaneo no venia con texto, es decir, que sea legible por maquina, asi que no puedo Ctrl+F-ear lo que quería buscar.

para lxs que no saben que es el OCR, es las siglas en ingles para "Optical Character Recognition", es decir, [Reconocimiento óptico de caracteres](https://es.wikipedia.org/wiki/Reconocimiento_%C3%B3ptico_de_caracteres). significa que La Computadora(tm) va a leer la imágen que le pasemos y devuelva texto que entienda la maquina, en este caso, incluyendo la posición de cada palabra para ponersela al PDF y que puedas copiar o buscar texto dentro de las imágenes.

buscando que software de OCR puedo usar, el más conocido que es libre es tesseract, pero no es para usar solo, es decir, es para que otros programas implementen, usando tesseract, un formato de archivo o una modalidad. y en eso encontré `pdfsandwich`, que es un programita sencillo que te hace todo el trabajo de procesar las imágenes, pasarlas a tesseract y guardarte el PDF con el texto legible por máquina.

para instalarlo, podés mirar [el sitio web del proyecto (en inglés)](http://www.tobias-elze.de/pdfsandwich/). en mi caso, como uso void linux, fue tan simple como poner el comando:
```sh
sudo xbps-install pdfsandwich
```
y seguir lo que decía en pantalla.

despúes, para procesar un PDF, simplemente corres el programa con la ruta del archivo como argumento, y hace la mágia:
```sh
pdfsandwich pdf-de-mierda.pdf
```

tené en cuenta que el proceso puede tardar bastante, y que va a (ab)usar los recursos de tu computadora mientras lo esté haciendo.
