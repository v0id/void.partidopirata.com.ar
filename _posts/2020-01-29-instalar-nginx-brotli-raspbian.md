---
layout: post
title: como instalar nginx_brotli en raspbian
date: 2020-01-29 00:00:00 +0000
categories:
  - guía
  - instalar
  - raspbian
  - nginx
  - brotli
  - optimización
---

esta es una guía para que no me olvide como instalar el modulo de brotli (un algoritmo de compresión) para nginx en raspbian.

1. entramos a `/etc/apt/sources.list` y descomentamos el `deb-src` para poder descargar el código fuente de nginx usado para compilar el paquete de raspbian
   ```sh
   sudo nano /etc/apt/sources.list # o tu editor favorito
   ```

1. actualizamos la base de datos de `apt`
   ```sh
   sudo apt update
   ```

1. vamos a un directorio temporal para poner el código fuente
   ```sh
   cd /tmp
   ```

1. y conseguimos el código fuente
   ```sh
   apt source nginx # el de nginx
   git clone --recursive https://github.com/google/ngx_brotli # el de el modulo
   ```

1. instalamos las dependencias para compilar nginx
   ```sh
   sudo apt install build-essential
   sudo apt build-dep nginx
   ```

1. entramos al directorio del código de nginx (reemplazar 1.14.2 con la version que genere)
   ```sh
   cd nginx-1.14.2/
   ```

1. conseguir parametros de compilación de nginx
   ```sh
   nginx -V
   ```
   de ahí te va a saltar algo así:
   ```
   nginx version: nginx/1.14.2
   built with OpenSSL 1.1.1c  28 May 2019 (running with OpenSSL 1.1.1d  10 Sep 2019)
   TLS SNI support enabled
   configure arguments: # bocha de cosas
   ```
   esa parte del final, que señalize con "bocha de cosas", hay que copiar para usar después.

1. configurar compilación de nginx
   ```sh
   ./configure <acá se pega lo que copiamos> --add-dynamic-module=../ngx_brotli
   ```
   ojo que si lo que pegamos tiene sus propio `--add-dynamic-module=<algo>`, hay que borrar esos y solo esos cosos para que compile bien, ya que apunta a ciertos modulos que nosotres no bajamos.

1. compilar modulo
   ```sh
   for soname in objs/ngx_http_brotli_{static,filter}_module.so; do
       make -f objs/Makefile $soname
   done
   ```

1. instalar modulo
   ```sh
   sudo su
   cp objs/*.so /usr/lib/nginx/modules/
   ```

1. activar modulo
   ```sh
   for name in ngx_http_brotli_{static,filter}; do
       # crea una config en nginx que carga el modulo
       echo "load_module \"modules/"$name"_module.so\";" > /etc/nginx/modules-available/$name.conf
       # activa la config
       ln -s /etc/nginx/modules-available/$name.conf /etc/nginx/modules-enabled/
   done
   ```

1. listo! mirá [la documentación](https://github.com/google/ngx_brotli/blob/master/README.md#configuration-directives) de la configuración del modulo para empezar a usarlo.

gracias a [NixCP](https://nixcp.com/brotli-compression-nginx/#Installing_Brotli_from_source) y a [Sutty](https://0xacab.org/sutty/containers/ngx_brotli/blob/master/APKBUILD) por los recursos que use para crear este post :)

```sh
exit # limpiamos la escena de crimen ;)
```
