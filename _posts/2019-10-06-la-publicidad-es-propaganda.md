---
layout: post
title: la publicidad es propaganda capitalista
date: 2019-10-06 00:00:00 +0000
categories: propaganda
---

hace unos días, viendo movimientos contrapublicitarios, me di cuenta de algo interesante: las publicidades siempre son propaganda capitalista.

no solo capitalista, a veces es machista, facista o más. pero siempre es formal, o como se dice "políticamente correcto".
quiere tomar control sobre nuestras cuerpas, y sobre nuestros hábitos de consumo, por supuesto. también siempre quiere ser intima, directa, hasta a veces incomodante.

y me di cuenta de esto también pensando como aprendí a diferenciar la publicidad de la propaganda, ya que la publicidad es comercial y la propaganda es generalmente política, pero las dos se muestran en los mismos espacios, y en los mismos medios. entonces, es diferente? lo mezclan a propósito, para que lo directamente político no se vea como tal? para forzarnos a ver esta propaganda? y llegué a esta conclusión.

también me di cuenta que las "propagandas" (de partidos políticos generalmente) también son publicidades: te venden la idea de una economía estable, un trabajo, [políticas machifacistas](https://invidio.us/watch?v=i-C5jC8MBps) entre otras cosas. te lo hacen por vos; de la misma manera que comprás verdura para no plantarlas vos, compras pan para no fabricarlo vos, etc.

eso nomás. les dejo unos linksitos de cosas anti-publicitarias lindas que encontré:
  - [democratic media please](https://democraticmediaplease.net/), cuenta como las publicidades atentan contra nuestra "democracia" representativa
  - [subvertisers international](https://subvertisers-international.net/), todavía no entendí muy bien que es pero el chabón de lo anterior es parte
  - [Adblock Bristol](https://twitter.com/AdblockBristol), hacen cositas en Bristol(?), parece que tienen alianza con XR

si encontrás algo copado mandamelo! salu3, nos vemos o/
