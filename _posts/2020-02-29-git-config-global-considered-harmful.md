---
layout: post
title: git config --global considered harmful
date: 2020-02-29 00:00:00 +0000
categories:
  - git
  - anonimato
---

hay un programa de versionamiento de proyectos llamado `git`. es muy bueno y muy útil para la organización de colaboración entre varias humanas (o cyborgs) para piezas de código.

cuando empezas a usar git y intentas hacer tu primer *commit* (un cambio en el repositorio git), git te dice esto:

```shell
$ git commit -m 'Hola mundo :)'

*** Por favor cuéntame quien eres.

Corre

  git config --global user.email "you@example.com"
  git config --global user.name "Tu Nombre"

para configurar la identidad por defecto de tu cuenta.
Omite --global para configurar tu identidad solo en este repositorio.

fatal: no es posible auto-detectar la dirección de correo (se obtuvo 'void@computadora.(none)')
```

generalmente, cualquier persona simplemente va a seguir las instrucciones, y hacer algo así:

```shell
$ git config --global user.email "v0id@riseup.net"
$ git config --global user.name "void"
```

esto generalmente no es un problema si la computadora que estoy usando solo la voy a usar para proyectos piratas. sin embargo, si yo fuese a estar trabajando en un proyecto de el laburo:

```shell
$ git config --global user.email "marcelo@gmail.com"
$ git config --global user.name "Marcelo Ciudadano Común"
```

entonces, cualquier commit que haga en cualquier repositorio git en esta computadora en el futuro, va a decir que viene de mi identidad como esta puesta en mi documento estatal.

si volvemos a leer el mensaje que nos dió git...

> `Omite --global para configurar tu identidad solo en este repositorio.`

podriamos hacer los mismos comandos sin `--global`, y configurandolo solo para este repositorio:

```shell
$ git config user.email "marcelo@gmail.com"
$ git config user.name "Marcelo Ciudadano Común"
```

después, en algún [proyecto pirata](//0xacab.org/pip/miniloom), ponemos lo otro:

```shell
$ git config user.email "v0id@riseup.net"
$ git config user.name "void"
```

y así, nos salvamos de auto-doxxearnos[^dox] cuando colaboramos en proyectos en los que preferimos tener otra identidad.

[^dox]: doxxear: publicar información personal en línea con la intención (o en este caso, sin la intención) de humillar/joderle la vida a alguien
