---
layout: post
title: linux es inseguro (parte 1)
date: 2021-05-24 00:00:00 +0000
categories:
  - seguridad
  - linux
---

_este post es el primero de una serie de tres, y tiene final de acción directa. ¡seguí leyendo!_

hay un supuesto en las comunidades de software libre, y aún afuera, de que linux es _más seguro_ que Windows o otros sistemas operativos. esto se debe a que Linux tiene poca parte del "mercado" y no lo usa mucha gente proporcionalmente a Windows, entonces lxs atacantes no se preocupan en producir malware destinados a Linux (aunque existen algunos, especialmente apuntando a servidores, ya que la gran mayoría de servidores corren Linux). de lo contrario, la seguridad en el _escritorio_[^1] GNU/Linux es más que nada un chiste; dejame explicar.

en la práctica, GNU/Linux todavía sigue el modelo de seguridad _de MS-DOS_[^2] en donde todos los programas que se ejecutan tienen acceso a todo: los archivos, el micrófono, la cámara, cuentas bancarias, entre otras cosas. lo único que se "proteje" es acceso a la cuenta de administradorx[^3] que aún así se puede fácilmente acceder interceptando la contraseña cuando se use esta cuenta.

lo único que nos proteje a la mayoría es nuestro navegador web, que llega a ser prácticamente un sistema operativo de por si; provee interfaces para que el código pueda acceder al sistema, corre assembly[^4] entre otras cosas. los problemas de dejar que el navegador tenga la responsabilidad de ser un pseudo-sistema-operativo son varios:

* el navegador es un programa muy complejo con doble digitos de millones de líneas de código.
    * esta complejidad fomenta el monopolio de implementación ya que nadie puede implementarlo por su cuenta excepto compañías grandes como Google y Mozilla que vienen hace mucho tiempo haciendolo; ni Microsoft pudo[^5].
    * también hay una relación directa entre complejidad y vulnerabilidades de seguridad.
    * la complejidad y la forma que está estructurada el navegador hace que sea imposible usar en computadoras viejas al ser demasiado lento. también te come la batería.
* las "aplicaciones" (sitios web) que corren sobre el navegador no tienen acceso a muchas cosas del sistema y tienen muchas restricciones, no permitiendo la implementación de cosas interesantes y colectivas como redes P2P[^6].

hay un impulso para que se creen tecnologías que usen funcionalidades del kernel para aislar las aplicaciones como [Flatpak](https://flatpak.org), sin embargo esta [falla miserablemente](https://flatkill.org) en la práctica dando acceso a demasiadas cosas, centralizando repositorios de programas y dejando dependencias importantes desactualizadas.

también hay que tener en cuenta que los sistemas privativos ("nuestra competencia") ya están avanzados en esto: macOS insiste en tener los programas descargados del App Store en su _sandbox_ [desde 2012](https://www.cnet.com/news/what-apples-sandboxing-means-for-developers-and-users/) y Windows hace lo mismo con las UWP y su respectivo _Store_.

en la parte dos voy a contarte sobre como aún si Flatpak fuera perfecto y lo usaramos para todo, igualmente fallaría como método de seguridad ya que el kernel mismo (_Linux en si_) se considera inseguro. en la parte tres, que podemos hacer como activistas y programadorxs para mejorar esta situación y apropiar nuestras computadoras.

[^1]: [¡necesitamos interfaces que nos hagan libres!](https://fauno.endefensadelsl.org/necesitamos-interfaces-que-nos-hagan-libres/)
[^2]: artículo en inglés: [The MS-DOS Security Model](https://theinvisiblethings.blogspot.com/2010/08/ms-dos-security-model.html)
[^3]: meme/webcomic en inglés: [XKCD 1200 "Authorization"](https://xkcd.com/1200/)
[^4]: vía [WebAssembly](https://webassembly.org/)
[^5]: con su navegador Microsoft Edge. ahora usa el mismo motor que Chromium, el navegador de Google. [EdgeHTML](https://es.wikipedia.org/wiki/EdgeHTML) era su motor.
[^6]: los navegadores soportan una interfaz que se llama WebRTC que permite la conexión directa entre dos computadoras no-servidoras. sin embargo, esta está diseñada para tener menos latencia al hacer cosas como audio y video llamadas. dependen si o si de una servidora centralizada [STUN y quizás TURN](https://developer.mozilla.org/es/docs/Web/API/WebRTC_API/Protocols) para conectarse. no está de más decir que <https://webrtc.org> es un sitio de Google.
