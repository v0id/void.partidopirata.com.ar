---
layout: post
title: rust es una mierda
date: 2019-12-30 00:00:00 +0000
published: false # me arrepentí xd
---

*bueno, tampoco tanto.*

estuve usando [rust](//rust-lang.org) para hacer una API, y me parece que al menos para esto, rust es una mierda por varios temas:

## rust es demasiado complejo

es como C++. tiene bocha de funcionalidades y cosas copadas, pero resulta que se hace increiblemente complejo de algunas maneras. principalmente, en tiempos de compilación es demasiado lento. me gusto esta comparación de cuanto tarda en compilarse unos lenguajes:

|  | espacio en disco | tiempo de compilación |
|--|------------------|-----------------------|
| Go | 525 MB | 1m 33s |
| **Rust** | **30 GB** | **45m** |
| GCC | 8 GB | 50m |
| Clang | 90 GB	| 25m |

no es tan lento como otros compiladores, pero Go es increiblemente más liviano (más sobre go después).

## rust es complicado de compilar

si usas *targets* poco comunes (como Linux con `musl` en vez de `glibc`) es complicadisimo compilar cosas por [problemas sin sentido](https://github.com/rust-lang/rust/issues/40174). aparte tarda mil años en compilar por cada intento.

## rust tarda mucho en compilar

eso. no hay mucho para explicar.

## rust tiene errores cripticos

con muchas librerias que intentan hacer mágia (como ORMs o *frameworks* web), es común encontrarse errores cripticos que no tienen sentido y que son larguisimos.

## conclusión

en realidad rust no es tan malo (:, tiene cosas copadas como abstracciones sin costo, el uso de memoria sin *garbage collector* y demás. pero por estas cositas y varias voy a estar probando go. veremos que pasa.
