---
layout: post
title: Facist-Droid
date: 2019-09-22 00:00:00 +0000
categories: f-droid
---

_este post está en inglés. si necesitas una traducción, avisame._

F-Droid is a free software app store for android. i use it every day.

if you didn't follow the facist fediverse scandal, the first paragraphs of F-Droid's [statement](https://f-droid.org/en/2019/07/16/statement.html) explains it pretty well.

in that statement, F-Droid says they "won’t tolerate oppression or harassment against marginalized groups", and therefore "won’t package nor distribute apps that promote any of these things." more specifically: "it won’t distribute an app that promotes the usage of [Gab]." but yet, they did. after this statement, after being told not to do so. there's two cases where this is explicit:

## FacistTusky ("FreeTusky")

"FreeTusky" is a Tusky fork where they've simply changed one thing: removed the blocklist. this blocklist only contains Gab domains, which of course, is a facist hell (just see their [wikipedia page](<https://en.wikipedia.org/wiki/Gab_(social_network)>).)

therefore, we can be pretty sure this is simply made to allow facists use Tusky. i'm pretty sure we can all agree with that.
however, F-Droid does not agree. see, [the ticket where they added this app](https://gitlab.com/fdroid/rfp/issues/1043) and [the ticket where they are told that of course, FreeTusky is for facists](https://gitlab.com/fdroid/fdroiddata/issues/1736) (they've locked this last one.)

this just goes to show that, yes: F-Droid supports facists.

## Facism As A Service ("Librem One")

"[Librem One](https://librem.one)" is a service by [Purism](https://puri.sm) that provides, along other things, a Mastodon instance as a service (they basically renamed a bunch of libre+gratis projects, hosted them and sold them as services.) they specifically removed anti-harrasment and anti-hate-speech tools like reporting, because their answer to that is ["Look after yourself"](https://librem.one/stay-safe/), a big "fuck-you" to the fediverse's philosophy.

F-Droid is [perfectly OK](https://gitlab.com/fdroid/fdroiddata/issues/1734) with packaging and distributing their apps.

as a plus, their "marketing director" is angry at [the riseup collective](https://riseup.net) because it apparently is "antifa email", and antifa is "a domestic terrorist organisation" (see: [this thread](https://todon.nl/@paulfree14/102266783570008200).)

## Facistlab ("Fedilab")

"Fedilab" originally did apply a blocklist that blocked Gab, but ended up removing it. this puts them in the "supports facists" list, pretty much. [this already mentioned ticket](https://gitlab.com/fdroid/fdroiddata/issues/1736) explains it pretty well.

# so, F-Droid? is it just words or are you actually gonna do something about facism?

i have made a [toot about it](https://todon.nl/@v0idifier/102594713688971710). consider retooting it to show support.

### GitLab CoC violation

someone [pointed out](https://bsd.network/@rootwyrm/102836833712898162) that what F-Droid is doing is violating GitLab's CoC and suggests emailing [conduct@gitlab.com](mailto:conduct@gitlab.com) until they do something. consider doing it if you agree.

### end

if you have any questions or info, please contact me so i can add it to this post. thank you.
