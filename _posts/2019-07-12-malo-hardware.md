---
layout: post
title: hardware choto y la magia de el software liviano
date: 2019-07-12 00:00:00 +0000
categories: low-tech
---

me cansé de usar hardware potente. si, lo dije: me aburrí de usar *computadoras que andan bien*.

por qué? porque es demasiado fácil. claro, instalo algo pesado y anda joya. descargo 30 contenedores
de docker y no pasa nada. no tengo que buscar soluciones livianas. simplemente funciona. realmente
quiero que sea así?

quizás si! o quizás no. o quizás quiero que ande mal pero bien. lento pero funcional. funcional pero
trabado. trabado pero útil. útil pero funciona.

no sé que quiero en general. pero ahora quiero una netbook mala, meterle una distribución liviana de
linux como [void linux](https://voidlinux.org/). y divertirme usando software liviano. y eso hice.

quizás no es tan difícil para mi; me acostrumbre a usar herramientas livianas. a veces, las prefiero
por encima de herramientas pesadas pero funcionales, aunque las pueda correr en mi computadora
"principal" perfectamente. entonces me acostumbre a usar vim, la terminal.

y ultimamente estuve aprendiendo a *hacer* esas herramientas y software liviano. me estuve copando
aprendiendo [rust](https://www.rust-lang.org/), que es un lenguaje de programación rápido y
eficiente. me gustaría hacer que mis herramientas sean livianas también. lo más liviano,
humanamente posible para mí.

pero aparte, haciendo todo esto, aprendo mucho. sobre como funcionan las computadoras. sobre como
medir velocidad. sobre software nuevo y bien diseñado.

que se yo, quizás soy solo yo, pero esta es una de mis locuras. quiero más software liviano #)

*PD: después de haber escrito todo este coso, me di cuenta que no lo hice en la netbook. mi excusa
es que estaba compilando algo, y se hace inusable cuando se esta compilando algo xP*
