---
layout: post
title: no quedan links, solo capturas
date: 2019-09-02 00:00:00 +0000
categories: links
---

google ganó. con el suceso de
[AMP](https://es.wikipedia.org/wiki/Accelerated_Mobile_Pages), lograron generar
la ilusión infinita: todo viene de google. (nota: al escribir esto, la versión
en español de esa página de wikipedia parece una publicidad). con publicidad.
sabemos que es una mierda, pero como "no se pueden bloquear anuncios en el
celu" ([es mentira](https://f-droid.org/en/packages/org.blokada.alarm)),
tenemos que comernos toda esa rica propaganda facista.

![una captura de pantalla de un articulo de BuzzFeed en AMP. en la barra
de URL, aparece google.com](assets/amp.jpg)

las personas con disminución visual o personas ciegas perdieron (nota: si hay un mejor termino, por favor avisame.).
las herramientas de accesibilidad perdieron. no solo lectores de pantalla,
sino también poder agrandar el texto de la pantalla (quiero hacer zoom carajo!)

todas son capturas. de twitter, de facebook. de instagram, de otras capturas.
_de pantallas_.

perdimos la verificación del hipervínculo. seguro, si vas a los tres puntos,
compartir, apretas en GMail :tm: y te lo mandas a vos misme quizás anda. pero
hasta esos links a veces no andan, tenés que iniciar sesión (loginwall!) para usarlos
(como las _stories_ de instagram). o te trackean por toda la web, o quien sabe
que cosa más.

perdimos la H y la T de HTML. los hipervínculos. la interoperabilidad. por
qué? porque ayudas a la competencia. porque no trae clicks y visitas. por que
no da plata.
