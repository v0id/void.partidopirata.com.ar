---
layout: post
title: como compilar dino.im en void linux
date: 2020-01-30 00:00:00 +0000
categories:
  - guía
  - instalar
  - dino
  - void-linux
  - xmpp
---

*update:* ahora que salió dino 0.1 hay [una PR](https://github.com/void-linux/void-packages/pull/18648) para que haya un paquete de dino en void de forma oficial.

[dino.im](//dino.im) es una aplicación de mensajería que usa el protocolo XMPP. es la más moderna que encontré. para instalarla en void linux hay que tener la lista de dependencias, así que me puse a buscar todas las dependencias para compilarlo y en el medio me encontre con ciertos problemas (especificamente, con `libsignal-protocol-c`). siguiendo esta guía no deberías tener ningún problema :)

1. instalar dependencias (para compilar y abrir dino)
   ```sh
   sudo xbps-install base-devel cmake ninja glib-networking {gettext,glib,gtk+3,gpgme,libgee08,libgcrypt,qrencode,libsoup,sqlite,vala}{,-devel}
   ```

2. clonar el repositorio de dino y entrar
   ```sh
   git clone https://github.com/dino/dino
   cd dino
   ```

3. configurar la compilación, pidiendo que se compile `libsignal` individualmente en vez de usar la del sistema.
   ```sh
   ./configure --with-libsignal-in-tree
   ```

4. compilar
   ```sh
   make
   ```

listo! los binarios van a estar en `build/`. <3
