---
layout: post
title: mis aventuras compilando android (LineageOS MicroG 17.1)
date: 2020-06-07 00:00:00 +0000
categories:
  - android
---

esto no es una guía, pero más que nada anoto cosas para tener en cuenta. para una guía (en inglés) para compilar LineageOS MicroG, mirar [el README del contenedor compilador](https://github.com/lineageos4microg/docker-lineage-cicd). más que nada, estas son unas notas técnicas para tener en cuenta

## advertencia

vas a necesitar una CPU relativamente rápida para que esto termine a un tiempo rasonable. menos potencia = más tiempo va a tardar. y más importante, vas a necesitar *como minimo* 8GB de RAM, y mucho swap. deberías tener ~24GB o más en total de memoria+swap.

tener un SSD también puede acelerar la compilación. también vas a necesitar espacio en disco, como minimo 70GB, aunque probablemente termines necesitando más de 100GB.

una conexión de internet rápida es recomendada, pero podés dejarlo bajando a la noche si es lenta.

## para compilar 17.1 ahora mismo

el contenedor oficial [todavía no soporta 17.1](https://github.com/lineageos4microg/docker-lineage-cicd/issues/68). podés compilar tu propio contenedor con [la PR para 17.1](https://github.com/lineageos4microg/docker-lineage-cicd/pull/70) así:

```sh
git clone https://github.com/SolidHal/docker-lineage-cicd.git
cd docker-lineage-cicd
git checkout lineageos-17.1
docker build -t docker-lineage-cicd-17.1 .
```

acordate de cambiar el nombre de la imágen de `lineageos4microg/docker-lineage-cicd` a `docker-lineage-cicd-17.1`.

## bypassear VPN

si usas una VPN en tu compu pero querés que se baje rápido el códgio, podes crear una red de docker que pasa directamente a la interfaz de red sin pasar por la VPN.

```sh
ip addr # encontrá tu interfaz de red
docker network create -d macvlan -o parent=$INTERFAZ_DE_RED --subnet 192.168.1.0/24 --gateway 192.168.1.1 red
# cambiar subnet/gateway acordemente
```

## swapiness

yo tengo bastaante RAM, así que tengo puesto la `swapiness` a 10 (de 100) para que casi nunca swappee.
sin embargo, al compilar, me pasaba que no swappeaba suficientemente rápido y me tiraba "Killed" (de que se le terminó la memoria).
temporalmente le cambie la swapiness a 70 así: `sudo sysctl vm.swapiness=70`

