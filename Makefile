all: push build deploy

push:
	git push

build:
	TZ=0 JEKYLL_ENV=production bundle exec jekyll build

deploy:
	rsync -rvzP _site/ void@partidopirata.com.ar:~/public_html/
